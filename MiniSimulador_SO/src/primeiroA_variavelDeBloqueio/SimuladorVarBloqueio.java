package primeiroA_variavelDeBloqueio;

public class SimuladorVarBloqueio {

	public static void main(String[] args){
	
		Turn turn = new Turn();
		int quantum = 5000;
	
		ProcessoBloqueio p0 = new ProcessoBloqueio(0, turn, 5000, quantum);
		ProcessoBloqueio p1 = new ProcessoBloqueio(1, turn, 10000, quantum);
		ProcessoBloqueio p2 = new ProcessoBloqueio(2, turn, 15000, quantum);
		ProcessoBloqueio p3 = new ProcessoBloqueio(3, turn, 20000, quantum);
		ProcessoBloqueio p4 = new ProcessoBloqueio(4, turn, 25000, quantum);
	
		p0.start();
		p1.start();
		p2.start();
		p3.start();
		p4.start();
	
	}
}
