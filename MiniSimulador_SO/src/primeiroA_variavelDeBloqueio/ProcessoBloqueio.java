package primeiroA_variavelDeBloqueio;

public class ProcessoBloqueio extends Thread {
	private int pid;
	private Turn turn;
	private int pTime;
	private int quantum;

	public ProcessoBloqueio(int pid, Turn turn, int pTime, int quantum) {
		this.pid = pid;
		this.turn = turn;
		this.pTime = pTime;
		this.quantum = quantum;
	}

	@Override
	public void run() {
		while (this.turn.getTurn() == 1) {
			// Espera ocupada!
			try {
				System.out.println("Processo " + this.pid + " em espera");
				sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		this.turn.setTurn(1);

		System.out.println("Processo " + this.pid + " executando a regi�o cr�tica!!");

		while (this.pTime > 0) {

			try {
				System.out.println("Processo " + pid + " sendo retirado da CPU");
				if (pTime < quantum) {
					sleep(pTime);
				} else {
					sleep(quantum);
				}
				this.pTime -= this.quantum;

				System.out.println("Processo " + pid + " voltando � CPU");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (pTime > 0) {
				System.out.println("Tempo remanescente do processo " + this.pid + " = " + this.pTime/1000 + " segundos");
			} else {
				System.out.println("Processo " + this.pid + " acabou!");
			}
				
		}

		this.turn.setTurn(0);
	}

}
