package escalonador;

import java.util.ArrayList;
import java.util.Scanner;

public class SimuladorEscalonador {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		int operacao;

		int quantum = 5000;

		Processo p1 = new Processo(1, 7000);
		Processo p2 = new Processo(2, 6000);
		Processo p3 = new Processo(3, 5000);
		Processo p4 = new Processo(4, 6000);
		Processo p5 = new Processo(5, 9000);
		Processo p6 = new Processo(6, 7000);
		Processo p7 = new Processo(7, 6000);
		Processo p8 = new Processo(8, 5000);
		Processo p9 = new Processo(9, 6000);
		Processo p10 = new Processo(10, 9000);
		ArrayList<Processo> processos = new ArrayList<Processo>();

		processos.add(p1);
		processos.add(p2);
		processos.add(p3);
		processos.add(p4);
		processos.add(p5);
		processos.add(p6);
		processos.add(p7);
		processos.add(p8);
		processos.add(p9);
		processos.add(p10);

		p1.start();
		p2.start();
		p3.start();
		p4.start();
		p5.start();
		p6.start();
		p7.start();
		p8.start();
		p9.start();
		p10.start();

		System.out.println("Digite o Escalonador que deseja executar \n 1 - Round Robin \n 2 - Loteria");

		operacao = entrada.nextInt();

		try {
			if (operacao == 1) {
				RoundRobin escalonadorRoundRobin = new RoundRobin(processos, quantum);
				escalonadorRoundRobin.escalonar();
			} else if (operacao == 2) {

				Loteria escalanodarLoteria = new Loteria(processos, quantum);
				escalanodarLoteria.escalonar();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		entrada.close();

	}

}
