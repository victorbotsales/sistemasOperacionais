package escalonador;

public class Processo extends Thread {
	private int pid;
	private int pTime;
	private int idDesliga; //Para fazer o 

	public Processo(int pid, int pTime) {
		super();
		this.pid = pid;
		this.pTime = pTime;
		this.idDesliga = pid;
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {

			try {
				// Print para testes, se botar sleep ir� bugar, pois ir� lan�ar
				// uma exce��o
				// System.out.println("Processo " + pid + " executando");

				synchronized (this) {
					// Se o idDesliga for igual ao id do processo referente, ele
					// ir� ficar em espera at� ser acordado pelo escalonador
					// (recebendo um notify)
					while (idDesliga == this.pid) {
						wait();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void acordar() {
		this.idDesliga = -1;
		notify();
	}

	/*
	 * SAIR CPU
	 */
	public synchronized void dormir() {
		this.idDesliga = this.pid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getpTime() {
		return pTime;
	}

	public void setpTime(int pTime) {
		this.pTime = pTime;
	}

	public void decrementTime(int quantum) {
		this.pTime -= quantum;
	}

	public void preempcao() {
		// TODO
	}

}
