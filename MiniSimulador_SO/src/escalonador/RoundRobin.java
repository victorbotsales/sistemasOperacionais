package escalonador;

import java.util.ArrayList;

public class RoundRobin {
	private ArrayList<Processo> processos = new ArrayList<Processo>();
	private int quantum;
	
	public RoundRobin(ArrayList<Processo> processos, int quantum) {
		super();
		this.processos = processos;
		this.quantum = quantum;
	}
	
	
	public void escalonar() throws InterruptedException{
		int size = processos.size();
		
		while(size>0){
			processos.get(0).acordar();
			System.out.println("Processo " + processos.get(0).getPid() + " inicia a execu��o no CPU!");
			if (processos.get(0).getpTime() < quantum) {
				Thread.sleep(processos.get(0).getpTime());
				processos.get(0).setpTime(0);
			} else {
				Thread.sleep(quantum);
				processos.get(0).decrementTime(quantum);
			}
			
			System.out.println("Tempo remanescente: " + processos.get(0).getpTime()/1000 + "s");
			if(processos.get(0).getpTime() > 0){
				System.out.println("Time out, processo " + processos.get(0).getPid() + " retirado da CPU!!");
				processos.get(0).dormir();
				roundRobin();
			}
			else{
				processos.get(0).interrupt();
				System.out.println("Processo " + processos.get(0).getPid() + " acabou!");
				processos.remove(0);
				size = processos.size();
				System.out.println("Tamanho da fila = " + processos.size());
				if(processos.size() == 0){
					break;
				}
			}
			
		}
	}
	
	private  void roundRobin(){
		Processo auxProc = processos.get(0);
		processos.remove(0);
		processos.add(auxProc);
	}
}
