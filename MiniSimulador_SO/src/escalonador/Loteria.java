package escalonador;

import java.util.ArrayList;
import java.util.Random;

public class Loteria {
	private ArrayList<Processo> processos = new ArrayList<Processo>();
	private int quantum;

	public Loteria(ArrayList<Processo> processos, int quantum) {
		super();
		this.processos = processos;
		this.quantum = quantum;
	}

	public void escalonar() throws InterruptedException {
		int size = processos.size();

		while (size > 0) {
			Random r = new Random();
			int pidSorteado = r.nextInt(size);
			// System.out.println(pidSorteado); Para testes
			processos.get(pidSorteado).acordar();
			System.out.println("Processo " + processos.get(pidSorteado).getPid() + " inicia a execu��o no CPU!");
			if (processos.get(pidSorteado).getpTime() < quantum) {
				Thread.sleep(processos.get(pidSorteado).getpTime());
				processos.get(pidSorteado).setpTime(0);
			} else {
				Thread.sleep(quantum);
				processos.get(pidSorteado).decrementTime(quantum);
			}

			System.out.println(
					"Tempo remanescente de processamento: " + processos.get(pidSorteado).getpTime() / 1000 + "s");
			if (processos.get(pidSorteado).getpTime() > 0) {
				System.out.println("Time out, processo " + processos.get(pidSorteado).getPid() + " retirado da CPU!!");
				processos.get(pidSorteado).dormir();
			} else {
				processos.get(pidSorteado).interrupt(); // Tempo de
														// processamento foi
														// esgotado, precisa
														// interromper a thread
														// para parar sua
														// execu��o dentro do
														// la�o no run()
				System.out.println("Processo " + processos.get(pidSorteado).getPid() + " acabou!");
				processos.remove(pidSorteado);
				size = processos.size();
				System.out.println("Tamanho da fila = " + processos.size());
				if (processos.size() == 0) {
					break;
				}
			}

		}
	}

}
