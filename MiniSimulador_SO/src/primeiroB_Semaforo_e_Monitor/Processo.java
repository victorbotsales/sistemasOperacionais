package primeiroB_Semaforo_e_Monitor;

public class Processo extends Thread {
	private Buffer buf;
	private char c;
	private int pTime;
	private int choice;

	public Processo(Buffer buf, char c, int pTime, int choice) {
		this.buf = buf;
		this.c = c;
		this.pTime = pTime * 1000;
		this.choice = choice;
	}

	public void run() {

		// Semaforo Mutex
		if (this.choice == 1) {
			buf.inserir(c, pTime);
		}
		// Monitor
		else {
			buf.inserir2(c, pTime);

		}

	}

}
