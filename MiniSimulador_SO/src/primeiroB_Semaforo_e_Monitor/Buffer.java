package primeiroB_Semaforo_e_Monitor;

import java.util.concurrent.Semaphore;

public class Buffer {
	private char[] buffer;
	private Semaphore semaforo;
	private int quantum = 5000;

	public Buffer(int tamSemaforo) {
		this.buffer = new char[20];
		this.semaforo = new Semaphore(tamSemaforo);
	}

	public Buffer() {
		this.buffer = new char[20];
	}

	// Usando Semaphore ( mutex )
	public void inserir(char c, int pTime) {
		try {
			// Verifica se alguma thread j� adquiriu o recurso, se sim, o
			// avaliablePermits() vai ser igual a 0
			if (semaforo.availablePermits() == 0) {
				System.out.println("Processo: " + c + " est� aguardando regi�o cr�tica!");
			}
			// Adquirir a permiss�o para entrar na regi�o cr�tica
			semaforo.acquire();
			System.out.println("Processo: " + c + " entrou na regi�o cr�tica");
			// Coloca no buffer o caracter(20vezes) que representa o ID do
			// Processo que est� na regi�o cr�tica
			// O tamanho inicial do buffer � zero e ele � incrementado para
			// armazenar o caracter ID
			for (int i = 0; i < 20; i++) {
				buffer[i] = c;
			}
			// Chama a fun��o imprimir que tambem faz parte da regi�o cr�tica
			imprimir(c, pTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Usando Semaphore ( mutex )
	public void imprimir(char c, int processingTime) throws InterruptedException {
		while (processingTime > 0) {
			// Se o tempo restante do processamento for menor que o quantum de 5
			// segundos, ser� "executado" realmente o tempo restante.
			//
			if (processingTime < quantum) {
				Thread.sleep(processingTime);
				processingTime = 0;
			} else {
				//
				Thread.sleep(quantum);
				processingTime = processingTime - quantum;
			}
			// Apelida/denomina "processo" com o nome de Dispositivo.
			System.out.println("Dispositivo " + c + " processa regi�o cr�tica, resta " + processingTime / 1000
					+ "s de processamento!");
		}

		System.out.println(this.buffer);
		System.out.println("Processo " + c + " saiu da regi�o cr�tica");
		semaforo.release(); // Libera a permiss�o do recurso (regi�o cr�tica)

	}

	// Usando MONITOR
	public void inserir2(char c, int processingTime) {
		System.out.println("Processo: " + c + " est� aguardando regi�o cr�tica!");
		// In�cio da regi�o cr�tica, determinada pela palavra "synchronized"
		// (MONITOR) e ele faz o controle do lock desse bloco (regi�o cr�tica).
		synchronized (this) {
			System.out.println("Processo: " + c + " entrou na regi�o cr�tica");
			for (int i = 0; i < 20; i++) {
				buffer[i] = c;
			}
			try {
				imprimir2(c, processingTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	// Usando MONITOR
	public synchronized void imprimir2(char c, int processingTime) throws InterruptedException {
		while (processingTime > 0) {
			if (processingTime < quantum) {
				Thread.sleep(processingTime);
				processingTime = 0;
			} else {
				Thread.sleep(quantum);
				processingTime = processingTime - quantum;
			}
			System.out.println("Dispositivo " + c + " processa regi�o cr�tica, resta " + processingTime / 1000
					+ "s de processamento!");
		}
		//Imprime o buffer
		System.out.println(this.buffer);
		System.out.println("Processo " + c + " saiu da regi�o cr�tica");
	}

}
