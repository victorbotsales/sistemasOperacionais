package primeiroB_Semaforo_e_Monitor;

import java.util.Scanner;

public class SimuladorMain {
	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		int operacao;

		System.out.println("Digite o algoritmo que deseja executar \n 1 - Semaforo Mutex \n 2 - Monitor");

		operacao = entrada.nextInt();

		if (operacao == 1) {
			// Semaforo (Mutex)
			Buffer buf = new Buffer(1);
			Processo processo1 = new Processo(buf, '1', 6, 1);
			Processo processo2 = new Processo(buf, '2', 8, 1);
			Processo processo3 = new Processo(buf, '3', 10, 1);
			Processo processo4 = new Processo(buf, '4', 13, 1);
			Processo processo5 = new Processo(buf, '5', 15, 1);
			processo1.start();
			processo2.start();
			processo3.start();
			processo4.start();
			processo5.start();

		} else if (operacao == 2) {
			// Monitor
			Buffer buf = new Buffer();
			Processo processo1 = new Processo(buf, '1', 6, 2);
			Processo processo2 = new Processo(buf, '2', 8, 2);
			Processo processo3 = new Processo(buf, '3', 10, 2);
			Processo processo4 = new Processo(buf, '4', 13, 2);
			Processo processo5 = new Processo(buf, '5', 15, 2);
			processo1.start();
			processo2.start();
			processo3.start();
			processo4.start();
			processo5.start();

		}

		entrada.close();

	}
}
