package primeiroA_alternanciaEstrita;

public class ProcessoAlternanciaEstrita extends Thread {
	private int pid;
	private Turn turn;
	private int quantum;
	private int pTime;
	int i = 0;

	public ProcessoAlternanciaEstrita(int pid, Turn turn, int quantum, int pTime) {
		super();
		this.pid = pid;
		this.turn = turn;
		this.quantum = quantum;
		this.pTime = pTime;
	}

	@Override
	public void run() {
		while (this.pid != this.turn.getTurn()) {
			// Espera ocupada!
			try {
				System.out.println("Processo " + this.pid + " em espera");
				sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("Processo " + pid + " entrando na CPU");

		while (this.pTime > 0) {

			try {
				System.out.println("Processo " + pid + " sendo retirado da CPU");
				if (pTime < quantum) {
					sleep(pTime);
				} else {
					sleep(quantum);
				}
				this.pTime -= this.quantum;

				System.out.println("Processo " + pid + " voltando � CPU");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (pTime > 0) {
				System.out.println("Tempo remanescente do processo " + this.pid + " = " + this.pTime/1000 + " segundos");
			} else {
				System.out.println("Processo " + this.pid + " acabou!");
			}
		}

		if (this.turn.getTurn() < 4) {
			this.turn.setTurn(this.turn.getTurn() + 1);
		} else {
			this.turn.setTurn(0);
		}

	}
}
