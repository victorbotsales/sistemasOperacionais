package primeiroA_alternanciaEstrita;

public class SimuladorAlternancia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Turn t = new Turn();
		int quantum = 5000;
		int pTime01 = 13000;
		int pTime02 = 12000;
		int pTime03 = 15000;
		int pTime04 = 22000;
		int pTime05 = 5000;

		ProcessoAlternanciaEstrita p0 = new ProcessoAlternanciaEstrita(0, t, quantum, pTime01);
		ProcessoAlternanciaEstrita p1 = new ProcessoAlternanciaEstrita(1, t, quantum, pTime02);
		ProcessoAlternanciaEstrita p2 = new ProcessoAlternanciaEstrita(2, t, quantum, pTime03);
		ProcessoAlternanciaEstrita p3 = new ProcessoAlternanciaEstrita(3, t, quantum, pTime04);
		ProcessoAlternanciaEstrita p4 = new ProcessoAlternanciaEstrita(4, t, quantum, pTime05);

		p0.start();
		p1.start();
		p2.start();
		p3.start();
		p4.start();

	}

}
